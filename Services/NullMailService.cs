﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleStore.Services
{
    public class NullMailService : IMailService
    {
        private readonly ILogger<NullMailService> _Logger;

        public NullMailService(ILogger<NullMailService> _logger)
        {
            _Logger = _logger;
        }

        public void SendMessage(string to, string subject, string body)
        {
            //log the message
            _Logger.LogInformation($"To: {to} Subject: {subject} Body: {body}");
        }
    }
}
