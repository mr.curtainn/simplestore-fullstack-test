﻿export interface Product {
    id: number;
    category: string;
    country: string;
    price: number;
    title: string;
}