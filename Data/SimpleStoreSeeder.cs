﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using SimpleStore.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleStore.Data
{
    public class SimpleStoreSeeder
    {
        private readonly SimpleStoreContext _ctx;
        private readonly IHostingEnvironment _hosting;
        private readonly UserManager<StoreUser> _userManager;

        public SimpleStoreSeeder(SimpleStoreContext ctx, IHostingEnvironment hosting, UserManager<StoreUser> userManager)
        {
            _ctx = ctx;
            _hosting = hosting;
            _userManager = userManager;
        }

        public async Task SeedAsync()
        {
            _ctx.Database.EnsureCreated();

            StoreUser user = await _userManager.FindByEmailAsync("robingordijn@gmail.com");
            if(user == null)
            {
                //create user
                user = new StoreUser()
                {
                    FirstName = "Robin",
                    LastName = "Gordijn",
                    Email = "robingordijn@gmail.com",
                    UserName = "robingordijn@gmail.com"
                };

                var result = await _userManager.CreateAsync(user, "P@ssw0rd");
                if (result != IdentityResult.Success)
                    throw new InvalidOperationException("Could not create new user in seeder");

            }

            if(!_ctx.Products.Any())
            {
                //create sample data
                //var filepath = Path.Combine(_hosting.ContentRootPath, "Data/art.json");
                //var json = File.ReadAllText(filepath);
                //var products = JsonConvert.DeserializeObject<IEnumerable<Product>>(json);

                var products = new List<Product>()
                {
                    new Product()
                    {
                        Price = 80,
                        Category = "Tecnology",
                        Country = "NL",
                        Title = "JBL Bluetooth Speaker"
                    },
                    new Product()
                    {
                        Price = 90,
                        Category = "Tecnology",
                        Country = "NL",
                        Title = "JBL Bluetooth Speaker 2.0"
                    }
                };
                _ctx.Products.AddRange(products);

                var order = _ctx.Orders.Where(o => o.Id == 1).FirstOrDefault();
                if(order != null)
                {
                    order.User = user;
                    order.Items = new List<OrderItem>()
                    {
                        new OrderItem()
                        {
                            Product = products.First(),
                            Quantity = 5,
                            UnitPrice = products.First().Price
                        }
                    };
                }

                _ctx.SaveChanges();
            }

        }
    }
}
